﻿using DotChain.Extensions;

namespace DotChain;

/// <summary>
/// The Block class is a main component into any Blockchain platform, 
/// it will store the data and act as a dataset for your application.
/// The class will expose a method to validate the data... The body of
/// the block will contain an Object that contain the data to be stored,
/// the data should be stored encoded.
/// All the exposed methods should return a Promise to allow all the methods
/// run asynchronous.
/// </summary>
public record Block
{
    public string? Hash { get; init; }
    public int Height { get; init; } = 1;
    public string? Body { get; }
    public long Time { get; init; }
    public string? PreviousBlockHash { get; init; }

    /// <summary>
    /// Constructor - argument data will be the object containing the transaction data
    /// </summary>
    /// <param name="data"></param>
    public Block(object data)
    {
        Body = data.ToHexString();
    }

    /// <summary>
    /// validate method will validate if the block has been tampered or not.
    /// Been tampered means that someone from outside the application tried to change
    /// values in the block data as a consecuence the hash of the block should be different.
    /// </summary>
    /// <returns></returns>
    public bool Validate()
    {
        var block = this with { Hash = null };
        return Hash == block.ToSha256();
    }

    /// <summary>
    /// Auxiliary Method to return the block body (decoding the data)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns>data</returns>
    public T? GetData<T>() => Body!.FromHexString<T>();
}